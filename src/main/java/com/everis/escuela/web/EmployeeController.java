package com.everis.escuela.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/business/v1")
public class EmployeeController {

	@RequestMapping("/employees/{id}")
	public String getEmployeeById(@PathVariable(value = "id") Long employeeId){
		
		return "ID Empleado: " + String.valueOf(employeeId);
	}
}
